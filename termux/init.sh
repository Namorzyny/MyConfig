#!/bin/bash

SCRIPT_ROOT=$(dirname $(readlink -f "$0"))

mkdir ~/.termux
cp $SCRIPT_ROOT/../termux/* ~/.termux
termux-reload-settings
termux-setup-storage
apt update && apt -y upgrade
apt -y install zsh curl wget neofetch openssl-tool vim
source $SCRIPT_ROOT/init-git

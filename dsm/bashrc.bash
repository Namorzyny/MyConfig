# use zsh if present
which zsh > /dev/null
if [ $? -eq 0 ]; then
    zsh
    exit
fi

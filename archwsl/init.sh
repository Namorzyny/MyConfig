#!/bin/bash

uname -a | grep Microsoft || exit 1

SCRIPT_ROOT=$(dirname $(readlink -f "$0"))

source $SCRIPT_ROOT/../archwsl/mirror.conf

echo "Server = $MIRROR_SERVER" > /etc/pacman.d/mirrorlist
pacman-key --init
pacman-key --populate
pacman -Syu base base-devel git
read -p 'Name: ' username
useradd -m -G wheel $username
passwd $username
chmod 600 /etc/sudoers
echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
chmod 400 /etc/sudoers
passwd -dl root
cmd.exe /c powershell arch.ps1 config --default-user $username
sudo -u $username $SCRIPT_ROOT/init-git
sudo -u $username $SCRIPT_ROOT/init-archwsl

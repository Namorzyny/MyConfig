#!/bin/bash

SCRIPT_ROOT=$(dirname $(readlink -f "$0"))

source $SCRIPT_ROOT/../repo.conf
source $SCRIPT_ROOT/../git.conf

git config --global core.eol lf
git config --global core.autocrlf input
git config --global user.name $GIT_NAME
git config --global user.email $GIT_EMAIL
git clone $REPO_HTTPS $LOCAL_PATH
cd $LOCAL_PATH
git remote remove origin
git remote add origin $REPO_SSH
source $SCRIPT_ROOT/restore-sshkey

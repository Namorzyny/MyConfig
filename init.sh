#!/bin/sh

# banner
echo
echo ' _____ ^  ^'
echo '|＼  /(´～`)＼     > 時は来た。'
echo '|　|¯¯¯¯¯¯¯¯¯¯|    > フレッシュピーチ...'
echo '|　|＝みかん＝|      ハートシャワー！'
echo ' ＼|__________|'
echo

# openwrt
if [ -f '/etc/openwrt_version' ]
then
    echo 'test'
fi

# msys2 / git for windows
if [ $? -eq 0 ]
then
    echo 'msys2'
fi

# synology dsm
uname -a | grep synology > /dev/null
if [ $? -eq 0 ]
then
    echo 'dsm'
fi

# wsl
uname -a | grep Microsoft > /dev/null
if [ $? -eq 0 ]
then
    # archwsl
    cat /etc/os-release | grep archlinux > /dev/null
    if [ $? -eq 0 ]
    then
        echo 'archwsl'
    fi
fi
